# Plano da Disciplina - Algoritmos e Programação de Computadores (113476)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva

## Período
2º Semestre de 2.023

## Turmas
* 09 - Fabricio Ataides Braz
* 10 - Nilton Correia da Silva

## Ementa
Princípios fundamentais de construção de programas. Construção de algoritmos e sua representação em pseudocódigo e linguagens de alto nível. Noções de abstração. Especificação de variáveis e funções. Testes e depuração. Padrões de soluções em programação. Noções de programação estruturada. Identificadores e tipos. Operadores e expressões. Estruturas de controle: condicional e repetição. Entrada e saída de dados. Estruturas de dados estáticas: agregados homogêneos e heterogêneos. Iteração e recursão. Noções de análise de custo e complexidade. Desenvolvimento sistemático e implementação de programas. Estruturação, depuração, testes e documentação de programas. Resolução de problemas. Aplicações em casos reais e questões ambientais.

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. 

Nosso foco é a necessidade de estimular a turma a adquirir habilidades fundamentais para o desenvolvimento de algoritmos. Em razão disso, decidimos lançar mão da instrução no início do período letivo. Momento em que passaremos pela sensibilização sobre lógica de programação, usando para isso os recursos de [programação em blocos](https://code.org), e depois fundamentos de [programação em Python](https://github.com/PenseAllen/PensePython2e/).


## Ferramentas & Materiais
* [Aprender3](https://aprender3.unb.br/course/view.php?id=14603) - Comunicação e trabalho colaborativo
* [Python](https://www.python.org/) - Linguagem de programação
* [Code](https://studio.code.org/join/LBJCDH) - Programação em blocos

**Ao se inscreverem no code informem em ``nome do usuário`` o seu email do aprender.unb.br**. Isso é para podermos consolidar as notas.

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir uma média final (MF) superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Composição da Média Final (MF)

As atividades avaliativas individuais que compõem a MF do aluno são: 

* PE1, PE2, PE3: Provas Escritas.  

* ExCODE: nota correspondente ao percentual de exercícios resolvidos na plataforma code.org

* ExAprender: nota correspondente ao percentual de exercícios resolvidos no próprio Aprender

![equation](https://latex.codecogs.com/svg.image?NSeq&space;=&space;0,1*ExCODE&space;&plus;&space;0,2*ExAprender&plus;&space;0,7*PE1)

![equation](https://latex.codecogs.com/svg.image?NDec&space;=&space;0,2*ExAprender&plus;0,8*PE2)

![equation](https://latex.codecogs.com/svg.image?NRep&space;=&space;0,1*ExAprender&plus;0,9*PE3)

![equation](https://latex.codecogs.com/svg.image?MediaFinal&space;=&space;\frac{NSeq&space;&plus;2*NDec&space;&plus;&space;3*NRep}{6})



### Cronograma
| Aulas   | Segunda   |   Semana | Feriado                 | Aula                                                                                                                                                                                                 |
|:--------|:----------|---------:|:------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1       | 28/08/23  |        2 | -                     | Acolhida                                                                                                                                                                                             |
| 2       | 29/08/23  |        3 | -                     | Code.org                                                                                                                                                                                             |
| 3       | 31/08/23  |        5 | -                     | Code.org                                                                                                                                                                                             |
| 4       | 04/09/23  |        2 | -                     | Introdução a sistema                                                                                                                                                                                 |
| 5       | 05/09/23  |        3 | -                     | Code.org                                                                                                                                                                                             |
| -       | 07/09/23  |        5 | Independência do Brasil | -                                                                                                                                                                                                  |
| 6       | 11/09/23  |        2 | -                     | [Introdução a programação](https://github.com/PenseAllen/PensePython2e/blob/master/docs/01-jornada.md)                                                                                               |
| 7       | 12/09/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 8       | 14/09/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| 9       | 18/09/23  |        2 | -                     | [Variáveis e expressões](https://github.com/PenseAllen/PensePython2e/blob/master/docs/02-vars-expr-instr.md)<br/>23:55 Prazo final para a entrega do Code.org                                        |
| 10      | 19/09/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 11      | 21/09/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| -       | 25/09/23  |        2 | Semana Acadêmica        | -                                                                                                                                                                                                  |
| -       | 26/09/23  |        3 | Semana Acadêmica        | -                                                                                                                                                                                                  |
| -       | 28/09/23  |        5 | Semana Acadêmica        | -                                                                                                                                                                                                  |
| 12      | 02/10/23  |        2 | -                     | [Funções](https://github.com/PenseAllen/PensePython2e/blob/master/docs/03-funcoes.md)<br/>[Funções com Resultado](https://github.com/PenseAllen/PensePython2e/blob/master/docs/06-funcoes-result.md) |
| 13      | 03/10/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 14      | 05/10/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| 15      | 09/10/23  |        2 | -                     | Prova Escrita 1 (PE1) - Estruturas Sequenciais                                                                                                                                                       |
| 16      | 10/10/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| -       | 12/10/23  |        5 | Padroeira do Brasil     | -                                                                                                                                                                                                  |
| 17      | 16/10/23  |        2 | -                     | [Estrutura de Decisão](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)                                                                                                |
| 18      | 17/10/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 19      | 19/10/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| 20      | 23/10/23  |        2 | -                     | [Estrutura de Decisão Aninhada](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)                                                                                       |
| 21      | 24/10/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 22      | 26/10/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| 23      | 30/10/23  |        2 | -                     | Prova Escrita 2 (PE2) - Estruturas de Decisão                                                                                                                                                        |
| 24      | 31/10/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| -       | 02/11/23  |        5 | Finados                 | -                                                                                                                                                                                                  |
| 25      | 06/11/23  |        2 | -                     | [Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md)                                                                                                |
| 26      | 07/11/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 27      | 09/11/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| 28      | 13/11/23  |        2 | -                     | [Listas](https://github.com/PenseAllen/PensePython2e/blob/master/docs/10-listas.md)                                                                                                                  |
| 29      | 14/11/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 30      | 16/11/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| 31      | 20/11/23  |        2 | -                     | [Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md)                                                                                                |
| 32      | 21/11/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 33      | 23/11/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| -     | 25/11/23  |        7 | Sábado 8h-9:50          | Estruturas de Repetições - Exemplos de Aplicações                                                                                                                                                    |
| -     | 25/11/23  |        7 | Sábado 10h-11:50        | Prática de Programação                                                                                                                                                                               |
| 34      | 27/11/23  |        2 | -                     | [Strings](https://github.com/PenseAllen/PensePython2e/blob/master/docs/08-strings.md)                                                                                                                |
| 35      | 28/11/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 36      | 30/11/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| -     | 02/12/23  |        7 | Sábado 10h-11:50        | Prática de Programação                                                                                                                                                                               |
| 37      | 04/12/23  |        2 | -                     | Estruturas de Repetições Aninhadas                                                                                                                                                                   |
| 38      | 05/12/23  |        3 | -                     | Prática de Programação                                                                                                                                                                               |
| 39      | 07/12/23  |        5 | -                     | Prática de Programação                                                                                                                                                                               |
| 40      | 11/12/23  |        2 | NEURIPS                 | Professor em Congresso                                                                                                                                                                               |
| 41      | 12/12/23  |        3 | NEURIPS                 | Professor em Congresso                                                                                                                                                                               |
| 42      | 14/12/23  |        5 | NEURIPS                 | Professor em Congresso                                                                                                                                                                               |
| 43      | 18/12/23  |        2 | -                     | Prova Escrita 3 (PE3) - Estruturas de Repetições                                                                                                                                                     |
| 44      | 19/12/23  |        3 | -                     | Prática de Programação                                                                                                                                                                              |
| 45      | 21/12/23  |        5 | -                     | Revisao de Nota    


## Referências Bibliográficas

* Básica 
    * [Pense Python](https://github.com/PenseAllen/PensePython2e/tree/master/docs)
    * Cormen, T. et al., Algoritmos: Teoria e Prática. 3a ed., Elsevier - Campus, Rio de Janeiro, 2012 
    * Ziviani, N., Projeto de Algoritmos com implementação em Pascal e C, 3a ed., Cengage Learning, 2010. 
Felleisen, M. et al., How to design programs: an introduction to computing and programming, MIT Press, EUA, 2001. 

* Complementar 
    * Evans, D., Introduction to Computing: explorations in Language, Logic, and Machi nes, CreatSpace, 2011. 
    * Harel, D., Algorithmics: the spirit of computing, Addison-Wesley, 1978. 
    * Manber, U., Introduction to algorithms: a creative approach, Addison-Wesley, 1989. 
    * Kernighan, Brian W; Ritchie, Dennis M.,. C, a linguagem de programacao: Padrao ansi. Rio de janeiro: Campus 
    * Farrer, Harry. Programação estruturada de computadores: algoritmos estruturados. Rio de Janeiro: Guanabara Dois, 2002.
    * [Plotly](https://dash-gallery.plotly.host/Portal)
    * [Portal Brasileiro de Dados Abertos](http://dados.gov.br)
    * [Kaggle](http://kaggle.com/)
    * [Ambiente de Desenvolvimento Python](https://realpython.com/installing-python/)
    * [Python Basics](https://www.learnpython.org)
    * [the Python Code Example Handbook](https://www.freecodecamp.org/news/the-python-code-example-handbook)

* Cursos de python
    - Os alunos podem escolher qualquer fonte extra de aprendendizagem para a linguagem Python. Os recursos são enormes. Algumas sugestões iniciais são:
    * https://www.pycursos.com/python-para-zumbis/
    * https://www.youtube.com/playlist?list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn
    * https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
    * https://www.youtube.com/watch?v=O2xKiMl-d7Y&list=PL70CUfm2J_8SXFHovpVUbq8lB2JSuUXgk
    * https://solyd.com.br/treinamentos/python-basico/
    * https://wiki.python.org/moin/BeginnersGuide/NonProgrammers

